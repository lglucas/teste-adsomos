<?php

use App\Http\Controllers\ClientesController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', [LoginController::class, 'loginForm'])->name('form.login');
Route::post('login', [LoginController::class, 'login'])->name('login');
Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->prefix('admin')->group(function () {
    // Dashboard route
    Route::get('/', [DashboardController::class, 'index'])->name('admin.index');

    // Clients routes
    Route::resource('clients', ClientesController::class, ['as' => 'admin'])->except('show');
});
