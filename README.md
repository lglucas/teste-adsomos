  

  

<p align="center"><img  src="https://www.adsomos.com/assets/images/logotipo-adsomos-softwares.png"  width="200"></p>

  

  

## Teste para a Adsomos

  

Projeto desenvolvido em Laravel 8.15

  

  

## Rodando o projeto

  

  

Seu ambiente deve respeitar as seguintes configurações

  

- PHP >= 7.3

  

- BCMath PHP Extension

  

- Ctype PHP Extension

  

- Fileinfo PHP Extension

  

- JSON PHP Extension

  

- Mbstring PHP Extension

  

- OpenSSL PHP Extension

  

- PDO PHP Extension

  

- Tokenizer PHP Extension

  

- XML PHP Extension

  

  

## Configure o seu .env

  

Crie uma cópia do .env-example

  

    cp .env-example .env

  
  

Configure sua conexão com o banco de dados


    DB_CONNECTION=mysql

    DB_HOST=127.0.0.1

    DB_PORT=3306

    DB_DATABASE=seu_banco

    DB_USERNAME=seu_usuario

    DB_PASSWORD=sua_senha

  

> Em DB_HOST, você pode definir o ip desejado caso o banco não esteja em
 localhost

  

Com o [composer](https://getcomposer.org/) instalado, na pasta raiz do projeto e em seu terminal, rode:

    composer install
    php artisan key:generate      
    php artisan migrate --seed 
    php artisan serve

1. Rode o composer para instalar o app

  

2. Gere a chave única do App

  

2. Rode a migration para adicionar os dados de teste e o **usuário padrão**

  

3. Rode a aplicação, você verá algo como

  

  

Starting Laravel development server: http://127.0.0.1:8000

  

  

Caso precise rodar a migration novamente, para evitar erros de banco rode:

    php artisan migrate:fresh --seed

  

## Usuário Padrão

  

  

Há uma autênticação no sistema, com apenas este usuário para os testes:

  

  

**email**: email@email.com

  

**senha**: 123456

  

  

## Especificações do sistema

  

Os dados que devem ser armazenados são:

  

• Código (Chave Primária serializada pelo banco de dados)

  

• Nome Completo (campo obrigatório)

  

• Data de Nascimento (campo obrigatório)

  

• CPF (campo obrigatório e único no sistema)

  

• Telefone

  

• E-mail

  

  

A aplicação deve fornecer as seguintes funcionalidades:

  

• Listar todos os clientes

  

• Inserir um novo cliente

  

• Atualizar um cliente

  

• Excluir um cliente

  

  

Além disso existem três regras de negócios que devem ser respeitadas:

  

• CPF não pode se repetir é um campo obrigatório.

  

• Telefone deve ser armazenado com máscara.

  

• E-mail deve ter uma validação se é valido ou não. Caso não seja avisar o usuário.

  

  

Quanto a interface:

  

• Apresentar uma mensagem de erro adequada quando alguma das regras de negócio não

  

for atendida

