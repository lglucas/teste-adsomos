<?php
namespace App\Services;

class MaskService
{
    public static function addMask($mask, $str)
    {
        $str = str_replace(" ", "", $str);

        $strLength = strlen($str);

        for ($i = 0; $i < $strLength; $i++) {
            $mask[strpos($mask, "#")] = $str[$i];
        }

        $masked = str_replace("#","", $mask);
        return $masked;
    }
  
    public static function removeMask($str)
    {
        return str_replace(['.', '-', '(', ' ', ')', '/', ','], '', $str);
    }
}
