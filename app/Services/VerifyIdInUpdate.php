<?php
namespace App\Services;


class VerifyIdInUpdate
{
  
    public static function getUpdateItemId($request, $model, $hash)
    {
        if (!$request->isMethod('put')) {
            return 0;
        }
        
        $item = $model->whereHash($hash)->first();
        
        return (!empty($item)) ? $item->codigo : 0;
    }
}
