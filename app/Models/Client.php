<?php

namespace App\Models;

use App\Services\MaskService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'telefone',
        'data_nascimento',
        'email',
        'cpf',
        'hash'
    ];

    // Set primary key name
    protected $primaryKey = 'codigo';
    
    public function create(array $attributes = [])
    {
        $attributes['hash'] = md5(uniqid(mt_rand(), true));
        $model = parent::create($attributes);
        return $model;
    }

    public function getDataNascimentoAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getCpfAttribute($value)
    {
        return !empty($value) ? MaskService::addMask('###.###.###-##', $value) : '';
    }

    public function setCpfAttribute($value)
    {
        $this->attributes['cpf'] = MaskService::removeMask($value);
    }
}
