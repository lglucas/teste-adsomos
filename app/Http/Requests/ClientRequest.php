<?php

namespace App\Http\Requests;

use App\Models\Client;
use App\Services\MaskService;
use App\Services\VerifyIdInUpdate;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Client $client)
    {
        $itemId = VerifyIdInUpdate::getUpdateItemId($this, $client, $this->client);
        
        $rules = [
            'nome' => 'required|string',
            'telefone' => 'string',
            'data_nascimento' => 'required|date',
            'email' => ['email', Rule::unique('clients')->ignore($itemId, 'codigo')],
            'cpf' => ['required', 'cpf', Rule::unique('clients')->ignore($itemId, 'codigo')],
        ];

        if(empty($this->email)) {
            unset($rules['email']);
        }

        if(empty($this->telefone)) {
            unset($rules['telefone']);
        }
        
        return $rules;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'cpf' => MaskService::removeMask($this->cpf),
        ]);
    }
}
