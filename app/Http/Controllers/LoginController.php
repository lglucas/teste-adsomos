<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('admin.index');
        } else {
            throw ValidationException::withMessages([
                'user' => [trans('auth.failed')],
            ]);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('form.login');
    }

    public function loginForm()
    {
        return view('auth.login');
    }
}
