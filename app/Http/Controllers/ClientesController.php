<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientRequest;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    /**
     * @var App\Models\Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $clients = $this->client->orderBy('codigo', 'DESC')->paginate(10);

        return view('admin.clientes.index')->with([
            'clients' => $clients,
            'viewTitle' => trans("Clients")
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clientes.create')->with([
            'model' => $this->client,
            'viewTitle' => trans("Create Client"),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {   
        try {
            $this->client->create($request->all());
            return redirect()->route('admin.clients.index')->with('alert', [
                'type' => 'success',
                'message' =>trans("Client Created!"),
            ]);
        } catch(\Exception $e) {
            return redirect()->route('admin.clients.index')->with('alert', [
                'type' => 'danger',
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($hash)
    {
        try {
            $client = $this->client->whereHash($hash)->first();
            if(empty($client)) {
                return redirect()->route('admin.clients.index')->with('alert', [
                    'type' => 'danger',
                    'message' =>trans("Client not found"),
                ]);
            }
        
            return view('admin.clientes.edit')->with([
                'model' => $client,
                'viewTitle' => trans("Edit").": ".$client->nome
            ]);
        } catch(\Exception $e) {
            return redirect()->route('admin.index')->with('alert', [
                'type' => 'danger',
                'message' =>trans("Something happened, try again latter"),
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientRequest $request, $hash)
    {
        try {
            $client = $this->client->whereHash($hash)->first();

            if(empty($client)) {
                return redirect()->route('admin.clients.index')->with('alert', [
                    'type' => 'danger',
                    'message' =>trans("Client not found"),
                ]);
            }

            $client->update($request->all());

            return redirect()->route('admin.clients.edit', $client->hash)->with('alert', [
                'type' => 'success',
                'message' =>trans("Client Updated!"),
            ]);
        } catch (\Exception $e) {
            return redirect()->route('admin.index')->with('alert', [
                'type' => 'danger',
                'message' =>trans("Something happened, try again latter"),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($hash)
    {
        try {
            $client = $this->client->whereHash($hash)->first();

            if(empty($client)) {
                return redirect()->route('admin.clients.index')->with('alert', [
                    'type' => 'danger',
                    'message' =>trans("Client not found"),
                ]);
            }
    
            $client->delete();
    
            return redirect()->route('admin.clients.index')->with('alert', [
                'type' => 'success',
                'message' =>trans("Client Deleted!"),
            ]);
        } catch (\Exception $e) {
            return redirect()->route('admin.index')->with('alert', [
                'type' => 'danger',
                'message' =>trans("Something happened, try again latter"),
            ]);
        }
    }
}
