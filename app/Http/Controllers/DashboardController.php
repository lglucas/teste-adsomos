<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.index')->with([
            'viewTitle' => trans("Dashboard"),
            'clientsCount' => Client::count()
        ]);
    }
}
