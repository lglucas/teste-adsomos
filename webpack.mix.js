const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/assets/js/app.min.js')
    .sass('resources/sass/app.scss', 'public/assets/css/app.min.css')
    .copyDirectory('resources/img', 'public/assets/img').version()

