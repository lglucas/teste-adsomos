<?php

namespace Database\Factories;

use App\Models\Client;
use App\Services\MaskService;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->faker->locale('pt_BR');

        return [
            'nome' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'data_nascimento' => $this->faker->date,
            'hash' => md5(uniqid(mt_rand(), true)),
            'cpf' => MaskService::removeMask($this->faker->cpf),
            'telefone' => $this->faker->phoneNumber
        ];
    }
}
