$('.js-mask-cpf').mask('000.000.000-00', {reverse: true})
$('.js-mask-phone').mask('(00) 0000-00000');
$('.js-mask-email').mask("A", {
	translation: {
		"A": { pattern: /[\w@\-.+]/, recursive: true }
	}
});