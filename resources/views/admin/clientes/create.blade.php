@extends('layouts.admin.master')

@section('content')
    @include('admin.clientes.form', ['action' => route('admin.clients.store'), 'edit' => false])
@endsection

@section('scripts')
@endsection
