@extends('layouts.admin.master')

@section('content')
    <div class="d-flex justify-content-end">
        <a class="btn btn-primary" href="{{route('admin.clients.create')}}">{{trans('Criar')}}</a>
    </div>

    <br>
    
    @if (! $clients->isEmpty())
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th> {{trans('Code') }}</th>
                        <th> {{trans('Name') }}</th>
                        <th> {{trans('CPF') }}</th>
                        <th> {{trans('Phone') }}</th>
                        <th> {{trans('Email') }}</th>
                        <th> {{trans('Birthday Date') }}</th>
                        <th> #</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clients as $client)
                        <tr>
                            <td>{{$client->codigo}}</td>
                            <td>{{$client->nome}}</td>
                            <td>{{$client->cpf}}</td>
                            <td>{{$client->telefone}}</td>
                            <td>{{$client->email}}</td>
                            <td>{{$client->data_nascimento->format('d/m/Y')}}</td>
                            <td>
                                <div class="d-flex">
                                    <a class="btn btn-info btn-sm mr-3" href="{{ route('admin.clients.edit', $client->hash) }}"> {{trans('Edit')}}</a> 
    
                                    <form class="m0" method="POST" action="{{ route('admin.clients.destroy', $client->hash) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-sm" href=""> {{trans('Delete')}}</button> 
                                    </form>
                                </div>
                                <!-- /.d-flex -->
                            </td>
                        </tr>                    
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
        {{$clients->links()}}
    @else
        <h4>{{trans('No data found')}}</h4>
    @endif
@endsection

@section('scripts')
@endsection
