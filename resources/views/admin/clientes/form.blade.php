<div class="row">
    <div class="col-md-12">
        <form action="{{$action}}" method="POST">
            <div class="form-group d-flex justify-content-end">
                <a  class="btn btn-light-outline mr-2" href="{{route('admin.clients.index')}}">{{ trans('Go Back') }}</a>
                <button class="btn btn-primary" type="submit">{{trans('Save')}}</button>
            </div>
            <!-- /.form-group -->

            @csrf
            @if ($edit)
                @method('PUT')
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nome" class="label">{{trans('Complete Name')}}</label>
                        <input id="nome" type="text" name="nome" class="form-control" value="{{ old("nome", $model->nome) }}">
                        @error("nome")
                            <small class="text-danger form-text">{{$message}}</small>
                        @enderror
                    </div>
                    <!-- /.form-group -->
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="data_nascimento">{{trans('Birthday Date')}}</label>
                        <input id="data_nascimento" name="data_nascimento" type="date" class="form-control" value="{{ old("data_nascimento", $model->data_nascimento->format('Y-m-d')) }}">
                        @error("data_nascimento")
                            <small class="text-danger form-text">{{$message}}</small>
                        @enderror
                    </div>
                    <!-- /.form-group --> 
                </div>
                <!-- /.col-md-3 -->
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="cpf">{{trans('CPF')}}</label>
                        <input id="cpf" name="cpf" type="text" class="form-control js-mask-cpf" value="{{ old("cpf", $model->cpf) }}">
                        @error("cpf")
                            <small class="text-danger form-text">{{$message}}</small>
                        @enderror
                    </div>
                    <!-- /.form-group --> 
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.row -->
         
   

            <div class="form-group">
                <label for="telefone" class="label">{{trans('Phone')}}</label>
                <input id="telefone" type="text" name="telefone" class="form-control js-mask-phone" value="{{ old("telefone", $model->telefone) }}">
                @error("telefone")
                    <small class="text-danger form-text">{{$message}}</small>
                @enderror
            </div>
            <!-- /.form-group --> 

            <div class="form-group">
                <label for="email" class="label">{{trans('Email')}}</label>
                <input id="email" type="email" name="email" class="form-control js-mask-email" value="{{ old("email", $model->email) }}">
                @error("email")
                    <small class="text-danger form-text">{{$message}}</small>
                @enderror
            </div>
            <!-- /.form-group --> 
        

          
        </form>
    </div>
    <!-- /.col-md-12-->
</div>
<!-- /.row-->
