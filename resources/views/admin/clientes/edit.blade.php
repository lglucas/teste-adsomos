@extends('layouts.admin.master')

@section('content')
    @include('admin.clientes.form', ['action' => route('admin.clients.update', $model->hash), 'edit' => true])
@endsection

@section('scripts')
@endsection
