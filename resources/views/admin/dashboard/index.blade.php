@extends('layouts.admin.master')

@section('content')

    <div class="row">
        <div class="col-md-3">
            <div class="dashboard-card card">
                <div class="card-body">
                    <p>{{ trans('Total of Clients') }}</p>
                    <h3 class="text-right">{{$clientsCount}}</h3>
                </div>
                <div class="card-footer d-flex justify-content-end">
                    <a class="btn btn-link text-right" href="{{route('admin.clients.index')}}"> {{trans('Access')}}</a>
                </div>
                <!-- /.card-footer -->
            </div>
        </div>
        <!-- /.col-md-3 -->
    </div>

@endsection

@section('scripts')
@endsection
