@extends('layouts.admin.auth')

@section('content')
<div class="row justify-content-center">
    <div class="col-lg-4 justify-content-center">
        <div class="d-flex my-4 justify-content-center">
            <img src="{{asset('assets/img/logo.png')}}" alt="Logo">
        </div>
    
        <form class="form-login" method="POST" action="{{route('login')}}">
            @csrf
            <div class="form-group">
                <label class="control-label" for="">Email</label>
                <input autofocus tabindex="1" class="form-control" type="text" name="email">
                @error('user')
                    <small class="text-danger form-text">{{$message}}</small>
                @enderror
                @error('email')
                    <small class="text-danger form-text">{{$message}}</small>
                @enderror
            </div>
            <!-- /.form-group -->
    
            <div class="form-group">
                <label for="password">{{trans("Password")}}</label>
                <input tabindex="2" class="form-control" type="password" name="password">
                @error('password')
                    <small class="text-danger form-text">{{$message}}</small>
                @enderror
            </div>
            <!-- /.form-group -->
            <button class="btn btn-primary btn-block" type="submit">{{trans('Login')}}</button>
         
        </form>
    </div>
    <!-- /.col-md-8 -->
  
</div>
<!-- /.row -->
  

@endsection
