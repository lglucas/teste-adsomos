<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('layouts.admin.components.admin-head-assets')
    </head>
    <body>
        @include('layouts.admin.components.admin-nav')
        <section id="app">
            <div class="page-wrapper">
                <div class="container mt-4">
                    @include('layouts.admin.components.admin-view-title')
                    @include('layouts.admin.components.admin-alerts')
                    @yield('content')
                </div>
                <footer class="footer d-none">
                    &copy; {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados
                </footer>
            </div>
        </section>
        @yield('scripts')
        <script src="{{ mix('assets/js/app.min.js') }}"></script>
    </body>
</html>
