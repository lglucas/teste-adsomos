@if (!empty($viewTitle))
<div class="row mb-3">
    <div class="col-md-12">
        <h3>{{$viewTitle}}</h3>
    </div>
    <!-- /.col-md-12 -->    
</div>
<!-- /.row -->
@endif