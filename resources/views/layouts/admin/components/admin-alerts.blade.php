@if (session('alert'))
<div class="row mb-3">
    <div class="col-md-12">
        <div class="alert alert-{{session('alert')['type'] ? session('alert')['type'] : 'warning'}} alert-dismissible fade show" role="alert">
            {{ session('alert')['message'] }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
             </button>
        </div>
        <!-- /.alert -->
    </div>
    <!-- /.col-md-12 -->
</div>
<!-- /.row -->
@endif
