<nav style="border-bottom: 1px solid #f2f2f2;" class="navbar navbar-light sticky-top py-1 bg-white navbar-expand-lg ">
    <div class="container">
        <a class="navbar-brand" href="{{route('admin.index')}}">
          <img src="{{asset('assets/img/logo.png')}}" alt="Logo">
        </a>    
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarNav">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
              <a class="d-md-inline-block nav-link" href="{{route('admin.clients.index')}}">{{ trans('Clients') }}</a>
              </li>
              <li class="nav-item">
                <a class="d-md-inline-block btn btn-outline-dark" href="{{route('logout')}}">{{ trans('Logout') }}</a>
              </li>
            </ul>
        </div>
    </div>
</nav>