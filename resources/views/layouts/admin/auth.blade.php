<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Teste Adsomos">
        <meta name="author" content="Lucas Guilherme">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base-url" content="{{ URL::to('/') }}">

        {{-- <link rel="icon" type="image/png" sizes="16x16" href="{{ mix('assets/img/favicon.png') }}"> --}}
        <title>{{ config('app.name') }}</title>

        @yield('styles')
        <link href="{{ mix('assets/css/app.min.css') }}" rel="stylesheet">
    </head>
    <body class="login-page">
        <section class="section-form">
            <div class="container mt-4">
                @yield('content')
            </div>
        </section>
        <footer class="footer text-center py-3">
            &copy; {{ date('Y') }} {{ config('app.name') }} - Todos os direitos reservados
        </footer>
        @yield('scripts')
        <script src="{{ mix('assets/js/app.min.js') }}"></script>
    </body>
</html>
